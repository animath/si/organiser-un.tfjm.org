# Organiser un TFJM²

Bienvenue sur le dépot de [organiser-un.tfjm.org](organiser-un.tfjm.org) !
C'est ici que vous pouvez contribuer directement à l'amélioration et au maintien de ce site.

Pour éditer du contenu, il suffit de se rendre dans le dossier `docs/` trouver le fichier
qui vous intéresse et cliquer sur le bouton **Éditer** en haut à droite.


## Construire une copie locale du site

Pour avoir une copie locale du site et faire des modification plus simplement,
il faut cloner le dépot :

Avec une configuration de Gitlab adéquate et vous clés:

```sh
git clone git@gitlab.com:animath/si/organiser-un.tfjm.org.git
```

Ou via https:

```sh
git clone https://gitlab.com/animath/si/organiser-un.tfjm.org.git
```

Après avoir changé de de dossier (`cd organiser-un.tfjm.org`), il faut installer 
`mkdocs` et `mkdocs-material`, soit avec [poetry](python-poetry.org):

```sh
poetry install
```

Soit avec pip:

```sh
pip install -r requirements.txt

```

Le site est ensuite disponible avec:

```sh
mkdocs serve
```

A cette addresse: [127.0.0.1:8000/](127.0.0.1:8000/).


## Remerciements

Merci à [@ddorn](gitlab.com/ddorn) pour la création du site ainsi qu'a l'ensemble du CNO de 2020
pour leur nombreuses contributions.

