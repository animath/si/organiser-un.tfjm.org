# Informations légales

## Publication du site

Le présent site www.organiser-un.tfjm.org est édité par *l’association pour
l’animation mathématique ANIMATH*, dont le siège est situé Institut Henri Poincaré,
11-13 rue Pierre et Marie Curie 75005 PARIS.

+ **Directeur de publication** : Monsieur Antoine MARTIN, Secrétaire général.
+ **Hébergeur** : OVH, 2 Rue Kellermann, 59100 Roubaix


## Politique de confidentialité

Conformément à la loi “informatique et libertés” du 6 janvier 1978 modifiée en 2004,
vous bénéficiez d’un droit d’accès et de rectification aux informations qui vous concernent,
que vous pouvez exercer en vous adressant à l’adresse électronique suivante :
<contact@tfjm.org>
ou par courrier postal :
Animath – Institut Henri Poincaré, 11-13 rue Pierre et Marie Curie 75231 PARIS Cedex 05

Les informations que vous fournissez sur ce site ne feront l’objet d’aucune
utilisation commerciale. Elles ne seront jamais communiquées à des tiers.


## Propriété intellectuelle

La structure générale, les logiciels, textes, images, vidéos, sons, savoir-faire,
animations, et plus généralement toutes les informations et contenus figurant dans le site
www.organiser-un.tfjm.org, sont la propriété d’Animath ou font l’objet d’un droit
d’utilisation ou d’exploitation assignment writing. Ces éléments sont soumis à la
législation protégeant le droit d’auteur.

Toute représentation, modification, reproduction, dénaturation, totale ou partielle,
de tout ou partie du site ou de son contenu, par quelque procédé que ce soit,
et sur quelque support que ce soit constituerait une contrefaçon sanctionnée
par les articles L 335-2 et suivants du Code de la Propriété Intellectuelle.

