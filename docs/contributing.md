# Contribuer à ce site

!!! todo
    Ajouter les instructions pour utiliser l'interface web de Gitlab, les issues
    et les MR ainsi que un petit tutoriel sur le Markdown (ou un lien).

    Éventuellement une description plus complète de comment installer le site en local.

## Markdown

Les pages de ce site sont intégralement écrites en *Markdown*. Le Markdown est un langage
très proche d'un simple texte, mais permettant une mise en forme très simplement et très
rapidement. On pourra se former au Markown en se rendant sur
[ce tutoriel très complet](https://www.markdownguide.org/basic-syntax).


!!! info "Markdown est partout"
    De part sa simplicité d'utilisation, de nombreux services utilisent Markdown. À titre
    d'exemple, les pages de [Wikipédia](https://fr.wikipedia.org/) ou encore les messages de
    Discord sont écrits en Markdown, avant d'être mis en forme correctement.

## Contribuer au projet

### Sur Gitlab

Le dépôt de ce site est disponible sur [Gitlab](https://gitlab.com/animath/si/organiser-un.tfjm.org).
Une fois authentifié, vous pouvez librement modifier le texte des pages, en ajouter, ...
Dès que les modifications sont soumises, une notification est envoyée automatiquement au
serveur lui indiquant de recharger les fichiers : il n'y a rien de plus à faire.
Néanmoins, merci de toujours donner un commentaire précis justifiant la modification :)

Chaque page dispose d'un raccourci placé à droite du titre de la page vers la page d'édition.

Il s'agit de la solution recommandée si vous n'êtes pas familier de Git.

### En local

#### Récupérer le dépôt

Pour des modifications plus avancées, vous pouvez également télécharger le projet sur votre
propre ordinateur et observer en temps réel les modifications.

Si vous disposez Git sur votre ordinateur (quel que soit son système d'exploitation), il
vous suffit de cloner le projet dans le dossier de votre choix :

```bash
git clone https://gitlab.com/animath/si/organiser-un.tfjm.org.git
```

ou bien via une connexion SSH si vous avez configuré votre clé :

```bash
git clone git@gitlab.com:animath/si/organiser-un.tfjm.org.git
```

Sinon, vous pouvez également télécharger le projet sous la forme d'une archive ZIP
[en cliquant ici](https://gitlab.com/animath/si/organiser-un.tfjm.org/-/archive/master/organiser-un.tfjm.org-master.zip),
et l'extraire là où vous voulez.

#### Démarrer le serveur de développement

Vous aurez ensuite besoin de dépendances pour pouvoir lancer le serveur.

À la racine du projet, vous disposez d'un mini-script d'installation (sous deux versions :
l'une pour un système Linux appelé `install.sh`, l'autre pour un système Windows appelé `install.bat`)
qui se chargera de récupérer les éventuelles dépendances manquantes.

Pour information, le script se contente d'invoquer :

```bash
pip3 install -r requirements.txt
```

Une fois cette étape réalisée, vous pouvez ensuite démarrer le serveur, via le script
`start-server.sh` (Linux) ou `start-server.bat` (Windows). Pour information, ce script
invoque `mkdocs server`. Cela a pour effet de démarrer un serveur de développement.
Sur votre navigateur, il vous suffit de vous rendre sur la page `http://localhost:8000`
pour naviguer localement sur le site. Attention : le site reste accessible uniquement
sur votre ordinateur et tant que la fenêtre de console reste ouverte. Fermez-la lorsque
vous avez terminé.

Le serveur de développement dispose d'une détection de modification des fichiers :
dès que vous modifiez une page, le serveur le détecte et la mise à jour est automatique,
vous n'avez même pas à recharger la page.

#### Soumettre vos modifications

Si vous n'êtes pas familier de Git, alors il vous est recommandé de copier-coller vos
modifications sur chacune des pages que vous avez modifié sur l'interface Web de Gitlab.

Sinon, si vous diposez de Git correctement configuré, il vous suffit de commiter, en
exécutant les insctructions suivantes dans votre terminal :

```bash
git pull	# On s'assure que la version de notre dépôt est à jour
git add .
git commit -m "Ma modification super trop bien"
git push
```

Vous aurez peut-être besoin avant d'avoir configuré votre Git en renseignant votre nom et
votre adresse e-mail afin de savoir que c'est bien vous qui avez effectué la modification :

```bash
git config --global user.name "Prénom Nom"
git config --global user.email "mon.adresse@email.fr"
```

Vous aurez peut-être besoin de rentrer des identifiants : ce sont ceux de Gitlab.

Un script côté serveur se charge ensuite de récupérer vos modifications et de mettre à
jour automatiquement l'affichage.

Vos modifications sont désormais visibles sur
[https://organiser-un.tfjm.org](https://organiser.un-tfjm.org) :)

#### Mettre à jour localement le projet

Attention : pour s'éviter tout problème, il est **impératif** de ne pas avoir effectué de
modifications localement, sinon vous allez tout casser si vous ne savez pas réparer.

Si vous n'utilisez pas Git, vous devez alors re-télécharger l'archive ZIP et la
re-décompresser. Inutile de réexcuter le script d'installation.

Sinon, assurez-vous de ne pas avoir de modifications (sauf si vous savez ce que vous faites).
Contrôlez en exécutant `git status`, réinitialisez vos modifications en exécutant
`git reset HEAD --hard`.

Il vous suffit d'exécuter `git pull` pour mettre à jour votre projet.


En cas de difficultés, ne pas hésiter à envoyer un mail à <contact@tfjm.org>.


Enjoy :)
