# Animath et le TFJM²

![Logo d'Animath](assets/logo-animath.png)

## L'association

L'association Animath est une association créée en 1998,
dont le but est de favoriser le goût et la pratique des mathématiques chez les jeunes.
Animath fédère les différentes entités qui partagent ces objectifs
et déploie elle-même des actions dans ce sens.

Animath permet chaque année à des milliers de collégiens et lycéens intéressés
par les mathématiques de participer à des activités complémentaires du cadre scolaire :
stages, compétitions nationales et internationales prestigieuses,
rencontres avec des chercheurs, etc. Ces activités existent à différentes échelles :
locale, nationale ou internationale. Une attention particulière est apportée à ceux qui,
pour des raisons d’origine sociale ou géographique, ou parce qu’il s’agit de filles,
ont tendance à moins s’engager vers les mathématiques, ainsi qu’aux plus jeunes.


Les actions d’Animath reposent sur un fort engagement bénévole pour organiser
les événements et surtout pour encadrer des projets scientifiques.
Qu’ils soient professeurs, chercheurs, ingénieurs, ou encore étudiants,
anciens bénéficiaires des actions de l’association ou non,
tous sont unis par la volonté de partager leur passion avec un large public.

!!! info ""
    [Plus d’informations sur Animath](https://animath.fr/)


## Animath et le TFJM²

Depuis 2011 et sa toute première édition, le tournoi est organisé par l’association Animath.
En 2015, avec la création des tournois régionaux, plusieurs acteurs locaux sont venus
participer à l’organisation et poursuivent depuis. Animath est aujourd'hui l’organisateur
principal du tournoi, responsable de l’organisation de la plupart des tournois régionaux et de la finale nationale.
L’association est surtout responsable de la coordination nationale du tournoi ce qui
inclut à la fois les questions pratiques et les questions pédagogiques,
notamment la rédaction et la publication des problèmes.


## L’implication de l’association

Au delà des nombreux bénévoles dédiés au tournoi que compte l’association,
Animath organise de nombreuses autres activités qui permettent d’enrichir le tournoi
de leurs expériences. La structure Animath fournit aussi un support en ce qui concerne
la gestion financière et administrative, la communication, les assurances, les locaux, le matériel, etc.

Animath apporte également une grande stabilité au tournoi grâce à une équipe
généralement plus expérimentée que celle du TFJM² et à une structure plus stabilisée.
