# La place du bénévole dans l'association

Les bénévoles sont l’essence d’une association, en particulier pour Animath qui compte très peu de salariés comparé aux centaines de bénévoles qui participent aux différentes actions de l’association. Le parcours des bénévoles est donc une préoccupation majeure de l’association. Plus que des ressources humaines, les bénévoles sont la force vive d’Animath.

## Donner de son temps

Être bénévole c’est avant tout donner de son énergie pour faire avancer une cause, en l’occurence ici celle de la recherche en mathématiques auprès des lycéens. Mais être bénévole c’est aussi choisir de s’engager librement, et donc choisir ses missions. Si certaines peuvent se présenter comme moins intéressantes, elles n’en sont pas moins indispensable à l’achèvement de la mission dans sa globalité. Et **il faut voir à travers chaque petite partie moins stimulante la réalisation du but** que tous poursuivent.

Donner de son temps et de son énergie est l’essence du bénévolat, et **certaines pratiques « trop généreuses » sont à éviter.** Ainsi, il convient de se faire rembourser les frais engagés pour l’association. N’achetez pas le goûter du tournoi par vous-mêmes en vous disant « j’aide l’association ». D’abord, parce que vous ne l’aidez pas tant que ça (les dépenses non-visibles peuvent biaiser les projections financières : vous ne serez pas toujours là). Ensuite, faites-vous rembourser le goûter et faites un don du même montant si vous y teniez, vous y gagnerez la défiscalisation !

Il est aussi **primordial de savoir dire « non ».** Chacun est conscient que tout le monde a un temps limité à accorder au tournoi. Il est préférable de refuser une mission que de s’engager à la faire et de ne pas en avoir le temps. Pour éviter ces situations, **restez régulièrement en contact avec le bénévole qui coordonne vos missions**, et surtout, **n’hésitez pas à lui signaler quand vous avez du mal à tenir les délais**, il pourra ainsi réagir et vous aider.

## Apprendre tout au long de son engagement

Outre le geste généreux du bénévole, il est important que chacun puisse apprendre de son engagement.
C’est là **une des plus grandes responsabilités dans l’encadrement des bénévoles** :
veiller à ce que chacun apprenne quelque chose.

Il y a deux manières principales d’apprendre en organisant le TFJM² :

 - **apprendre par le terrain**, en se lançant dans une tâche, en affrontant les défis
    qui se présentent et en sachant en tirer des leçons.
 - **apprendre de ses pairs**, ou de ses « aînés » grâce à la transmission continue
    de connaissance entre anciens organisateurs et nouveaux organisateurs.

**La transmission des connaissances est du devoir de chacun :**
les anciens doivent la partager avec les nouveaux, 
mais les nouveaux doivent aussi faire preuve de curiosité et questionner leurs prédécesseurs.

## Progresser en responsabilités

Le tournoi offre une multitude de façon de s’engager.
Il est possible de les rassembler par thème. Chaque bénévole peut évoluer
vers plus de responsabilité au sein du thème dans lequel il s’est engagé au fil des années.
Avoir plus de responsabilités c’est apprendre plus de choses, et des éléments différents.

Tous les bénévoles sont encouragés à s’engager sur deux tableaux,
**il est important de maintenir un équilibre entre les missions d’organisation,
et les missions proprement liées aux mathématiques.**

#### Au niveau régional

 - être bénévole le jour J, porter des cartons, aider à ce que tout se passe bien.
 - être en charge d’une mission pour le tournoi régional, la préparer en amont,
    la réaliser au bon moment (ex : être en charge des goûters, faire la lise de courses,
    les courses, et gérer leur distribution).
 - piloter l’organisation régionale du tournoi.

#### Au niveau national pour l’organisation

 - aider le Comité national d’organisation pour des missions ponctuelles
    (ex : distribuer des flyers, aider sur le site internet, etc.)
 - être membre du Comité national d’organisation, être en charge d’une mission
    (ex : le secrétariat et répondre et distribuer les questions de toutes les équipes sur l’organisation du tournoi)
 - avoir un poste clé au Comité national d’organisation et porter une partie du tournoi
    sur ses épaules (ex : assurer la bonne communication et organisation avec toutes les régions)

#### Sur les aspects pédagogiques

 - faire partie du jury pour les tournois régionaux
 - faire partie du jury pour la finale
 - soumettre des idées de problèmes
 - faire partie du Comité scientifique, fournir des idées de problèmes, les rédiger et les formaliser

## Être représenté dans les instances

> Animath est une association. Pour y être dument représenté il faut y **adhérer**,
> via cette page : [www.animath.fr/adhesion/](https://www.animath.fr/adhesion/).
> Être adhérent donne entre autre une voix lors de l’Assemblée générale de l’association.

C’est l’**Assemblée générale** qui élit le **Conseil d’Administration** (CA).
Le CA établit les grandes lignes de la politique de l’association et contrôle la réalisation.
Au sein du CA est élu le **Bureau**. C’est le Bureau qui conduit la gestion
de l’association au jour le jour, avec en tête le Président d’Animath.

L’expérience comme organisateur du TFJM² est utile et reconnu pour faire partie
du Conseil d’administration. Elle donne aussi des méthodes de travail et une expérience
seront un atout pour un futur membre du Bureau de l’association.

Pour être représentés, il faut certes adhérer pour pouvoir élire ses représentants,
mais aussi voir que **le parcours d’organisateur de TFJM² peut se prolonger
au sein d’Animath au service de toutes les actions de l’association.**

## Valoriser son engagement

L’engagement bénévole au sein du tournoi, et en particulier dans des postes
à responsabilité (organisateurs principaux des tournois régionaux, membres du
Comité national d’organisation, membre du Comité scientifique) sont des éléments
qui pourront vous **faire sortir du lot** dans bon nombre de situations.

**Pour les professionnels déjà avancés dans leur carrière**, c’est l’occasion
de démontrer (même si souvent ce n’est plus à prouver) leur engagement pour la promotion
de la recherche et des mathématiques auprès du jeune public. Vous pourrez ainsi
vous différencier quelque peu, mais aussi attirer la lumière sur votre équipe de recherche,
votre laboratoire en jouant finement.

**Pour les étudiants**, le fait d’avoir été à la manœuvre,
au pilotage d’une action de cette envergure peut être un point très utile dans vos
**lettres de motivations**, sur vos **CV** (Animath possède un compte **LinkedIn**,
signalez votre expérience !), dans un entretien pour un stage, un premier emploi
ou un concours d’admission. Le fait d’avoir participé à un élément de politique publique
pour la recherche est en soi un argument,
mais **expliquer ce que vous avez fait et ce que vous avez appris** vous donnera un
point de départ pour démontrer ce que vous avez en plus des autres candidats.

