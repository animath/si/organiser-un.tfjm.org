# Ressources documentaires locales

Voici une liste de documents utiles à l'organisation d'un tournoi.


 - Une checklist détaillé sur les poits d'organisation, de J-7 au jour J :
[TFJM-J-7aJJ.pdf](https://www.organiser-un.tfjm.org/wp-content/uploads/2019/01/TFJM-J-7aJJ.pdf)
 - Les fiches de synthèse, au format [PDF](https://inscription.tfjm.org/static/Fiche%20synth%C3%A8se.pdf)
    et le [tex original](https://inscription.tfjm.org/static/Fiche%20synth%C3%A8se.tex)

!!! todo

    Ajouter:

       - Tableaux pour rentrer les notes
       - Authorisations parentales, droit à l'image
       - Fiche notes du jury
       - Mails type
       - Feuille de calcul de budget ?
