# Recherches de finances au niveau local

La présence de partenaires conditionne la bonne santé du tournoi. Il est sage de se reposer sur plusieurs partenaires. Avec moins de 3 partenaires, le risque est important si l’un d’eux ne peut plus aider le tournoi (généralement, les fins de partenariats ne sont pas choisies par le partenaire mais par des contraintes venant d’au dessus). Avec plus de 6 partenaires, les choses deviennent réellement complexes à gérer.

> Tout au long de la démarche, il faut garder à l’esprit qu’un partenariat, c’est bien plus que de l’argent ! Un partenaire aide aussi à former le jury. Souvent un partenariat en entraîne un autre. Les partenariats sont aussi un premier pas de communication. Et plus qu’autre chose, un bon partenariat permet au tournoi de s’enrichir de l’expérience de son partenaire.

La recherche de partenaires est un processus assez méthodique. Il ne faut pas se mentir, un bon carnet d’adresses rendra les choses beaucoup plus simples. Mais on peut aussi réussir à partir de quelques contacts, de volonté et de travail.

## Recenser les partenaires potentiels

### Structures d’enseignement supérieur

Universités et écoles sont très souvent le premier pas du tournoi. Souvent établissement des organisateurs (qu’ils en soient élèves ou employés), ce sont les premiers contacts et généralement la porte ouverte vers les autres. Il est important de ne pas se limiter au campus où aura lieu le tournoi : aller chercher d’autres établissements dans la même ville, voire plus loin dans le département ou dans la région permet de mieux toucher le territoire.

Premier point à négocier avec ce type de partenaires : la disponibilité et l’accès aux locaux. Une ressource qui n’a pas forcément grande valeur pour eux, mais qui est indispensable au tournoi !

Second point, le jury : enseignants chercheurs et doctorants sont autant de membres possibles, impliquer l’établissement dans le jury est un gage de solidité et de pérennité du tournoi. Les bonnes personnes à inviter pur cela sont suffisamment haut placées pour être une « recommandation » pour le tournoi, suffisamment accessible pour avoir le temps.

Pour ce qui est du financement, le chemin peut être assez long. Adressez-vous assez haut dans la structure, le principal problème est souvent d’identifier la ligne budgétaire qui n’est pas déjà épuisée et qui pourrait convenir au TFJM². Pour ceci vous aurez besoin d’un interlocuteur avec une vision suffisamment globale. La direction des départements, de la recherche ou de l’enseignement sont souvent les bons organes.

### Laboratoires de recherche

Les laboratoires sont souvent l’étape d’après dans les contacts partenaires. Il est important de ne pas se limiter au campus où aura lieu le tournoi : aller chercher d’autres laboratoires dans la même ville, voire plus loin dans le département ou dans la région permet de mieux toucher le territoire.

Les laboratoires sont indispensables pour avoir des jurys de qualité et c’est d’abord de ceci dont il faut parler avec eux.

Une ressource assez commune pour les laboratoires très investis repose sur les doctorants. Ces derniers doivent en effet accomplir pour leur formation des heures en dehors de leur « contexte classique ». Certains laboratoires peuvent proposer à un ou deux doctorants d’effectuer ces heures au service du tournoi.

Enfin en ce qui concerne un appui financier, la plupart des laboratoires ont une mission de diffusion scientifique qui s’accorde bien avec le TFJM². Des crédits peuvent ainsi être débloqués. En ce qui concerne les _Unités mixtes de recherche_, bien souvent, elles doivent choisir d’orienter plutôt sur le CNRS, ou plutôt sur l’autre organisme. Les deux possibilités conviennent tout à fait.

### Laboratoires d’excellence

Les laboratoires d’excellence (ou LabEx) sont des structures qui souvent sont « hébergées » par une institution et sont financées par le programme Investissements d’avenir. Ces structures ont souvent plus facilement la capacité de débloquer des fonds que les laboratoires ordinaires. Elles sont souvent en étroite synergie avec un ou plusieurs laboratoires, aussi les contacts peuvent être simplifiés. Rechercher les LabEx autour d’un tournoi est donc une étape indispensable.

### Fondations d’écoles ou d’université

La quasi totalité des établissements scolaires ont une ou plusieurs fondations de l’établissement directement ou de ses anciens. Il est possible est commun que de tels fondations possèdent des orientations précises, par exemple :

 - la promotion de l’établissement.
 - la promotion et le soutien de la recherche scientifique.
 - la médiation scientifique auprès de jeunes publics.
 - la diffusion des sciences auprès des jeunes de milieux moins favorisés.

C’est autour de ces axes (après avoir recherché quels sont les objectifs précis de la fondation concernée), qu’il faut travailler et montrer en quoi le tournoi participe de ces missions.

### Fondations scientifiques

Dans certaines régions, il existe des fondations scientifiques, plus ou moins publiques. Ces contacts peuvent être intéressants. Il faut analyser la situation dans son contexte et sa particularité. Trop peu de généralités se dégagent essentiellement car de telles fondations sont peu nombreuses.

### Collectivités territoriales

Les collectivités territoriales sont un interlocuteur qu’il ne faut surtout pas négliger dans la construction du projet. Leur impact est essentiellement financier dans la mesure où leur vie quotidienne est assez éloignée des problématiques du tournoi. On peut simplifier quelque peu le maillage territorial pour mieux se représenter kes choses.

#### Communes

Les communes ne sont a priori pas compétentes pour les sujets dont traite le tournoi. Cependant, dans les grandes villes, il est possible qu’une mission dédiée existe. Il est alors utile de repérer l’adjoint au maire en charge de ces sujets et d’échanger avec ses services.

Il est aussi possible que le tournoi puisse entrer dans le cadre de la politique de la ville. Une étude de ce qui est mis en place est nécessaire pour s’en rendre compte. Ce peut être chronophage et a peu de chances d’être fructueux (les crédits alloués à cette mission sont souvent déjà dépensés par des actions plus « prioritaires »)

#### Établissements publics de coopération intercommunale

Les EPCI ne sont pas supposés être compétents en matière d’enseignement supérieur ou de culture scientifiques. Mais les grands établissements ont souvent envie de participer à cette tâche. Il n’est donc pas rare de trouver un vice-président en charge de l’enseignement supérieur. Ce peut être un bon contact à engager !

#### Départements

Les départements n’ont pas dans leur définition de mission concernant les champs d’action du tournoi (encore moins qu’à l’échelon communal) et leurs finances sont généralement les plus contraintes de outes les collectivités. Jusqu’ici, aucun partenariat n’a été lié avec un département pour le tournoi. Mais dans le cas où un signe positif viendrait à se montrer, il faut essayer.

#### Régions

Les régions ont une compétence en matière d’enseignement supérieur et de recherche, ainsi que d’éducation. Elles sont d’une part un acteur du monde universitaire, d’autre part elles ont une place grandissante dans le fonctionnement des lycées. En ce sens, les régions constituent l’échelon territorial pertinent pour le tournoi et il ne faut pas manquer de les associer au projet (ou du moins d’essayer). Il existe généralement des circuits officiels via des appels à projet de culture scientifique et technique, mais ces derniers ont souvent une temporalité qui leur est propre, il faut donc s’y prendre très en amont.

### Structures scientifiques locales

Que ce soient des associations porteuses de la culture scientifique et technique dans la région ou bien des structures plus ciblées, il est toujours bénéfique pour le tournoi de créer des liens avec d’autres acteurs locaux. Il y a très peu de chance que des ressources de financement en ressortent, mais l’expérience de ces acteurs et leurs relais de communication peuvent être déterminants pour le tournoi.

### Entreprises

Les partenariats avec les entreprises ne répondent à aucune généralité en raison de la diversité des entreprises et des voies d’accès qu’on emprunte pour entrer en contact avec elles. On peut tout de même dégager les entreprises les plus pertinentes qui sont généralement celles ancrées localement et avec une forte valeur ajoutée mathématique, généralement employant un certain nombre de mathématiciens.

Le départements de recherche et développement sont de bons points d’entrée. Il peut être plus simple de se rapprocher des PME que des grandes entreprises : plus petit dit souvent plus flexible. Il faut se créer un point d’entrée dans l’entreprise, qui généralement est un contact d’un des organisateurs.

## Candidature

Dans une certaine mesure, tous les processus de candidature se ressemblent. Il y a un dossier à constituer, ce dossier est remis entre les mains d’une personne ou d’un groupe qui délivre les financements.

Les contacts avec des membres du partenaire potentiel pourront toujours vous aider. Tout d’abord, pour constituer un dossier pertinent, trouver les bonnes voies pour le faire avancer, mieux cerner les priorités de l’organisme en question, vous éviter les pièges de débutants. Un bon contact servira d’appui à votre dossier grâce à une forme de caution, qu’elle soit implicite ou explicite. Un excellent contact fera partie de la commission qui attribue les fonds et plaidera votre cause.

Quelques éléments sont importants à garder à l’esprit :

 - ne jamais mentir ou être insincère dans une démarche avec un partenaire.
 - toujours éclairer le tournoi avec la lumière du partenaire, accentuer les points qui intéressent le partenaire, moins creuser ceux qui ne sont pas sa priorité.
 - veiller à la cohérence des budgets et à ce qu’ils soient fiables.

### Préparer un dossier

Avant toute chose, il est important de consulter le « cahier des charges » du dossier. Dans les cas simples, il est explicite dans un appel à projets. Sinon, il peut exister des documents plus lapidaires qui recensent les différents éléments. Si aucun appel formel n’existe, il faudra parvenir à rassembler les informations en échangeant avec des contacts.

Dans n’importe lequel des cas, un certain nombre d’informations :

 - axes d’intérêt du partenaire potentiel.
 - anciens projets lauréats de l’appel.
 - fourchette de subvention accordées.
 - documents de présentation du projet nécessaires.
 - documents financiers nécessaires.
 - documents administratifs nécessaires.

#### Présentations générales

> Attention : la taille de ces documents peut être encadrée, il faut veiller à respecter cet encadrement.

Pour ce qui est de la présentation globale du tournoi, il y existe des documents nationaux « prêts à l’emploi ». Il ne faut pas les utiliser à l’aveugle, mais ceci fait une base de travail. Il est important de saisir les points qui intéressent particulièrement le lecteur et de les développer plus en profondeur.

Il est toujours important de consacrer une partie significative au tournoi local, à ce qui fait sa particularité, à sa façon de s’intégrer dans l’environnement régional.

#### Budget

Il est important de vérifier la cohérence entre le projet et le budget. Il faut aussi que la somme demandée soit bien en accord avec le budget.

!!! todo
    [Plus d’informations sur le budget prévisionnel et sa création](../budget.md)

#### Pièces administratives

Pour ce qui est de ces pièces, généralement, il n’y a rien à créer. Le travail consiste essentiellement à faire la liste des pièces demandées, à les rassembler et à les joindre.

### Trouver un contact

Il est assez difficile d’obtenir des subventions sans contact à l’intérieur de l’organisme. Ce n’est nullement une question de favoritisme, il ne faut pas s’y méprendre. Les contacts au sein de l’organisme partenaire servent surtout à mieux cerner ce qui compte pour le partenaire et à adapter la demande à ses particularités.

Première façon de rechercher un contact, recensez les amis et collègues proches des membres de comité d’organisation. Cette phase est souvent fructueuse. Seconde étape, en parler avec le partenaire principal du tournoi. Souvent, le milieu des potentiels financeurs est assez restreint, il sera assez facile pour un partenaire de confiance de vous introduire auprès d’un nouveau partenaire.

Troisième étape : il y a toujours une « prime à l’optimisme » dans ces démarches. Donc tout ce qui peut créer des contacts est le bienvenu, l’imagination est la seule limite. Attention, il faut tout de même rester dans les limites du raisonnable et ne pas compromettre ses chances dans l’avenir.

### Se lancer

Le plus important pour se lancer : y penser très tôt ! Constituer son dossier en lien avec son contact. Selon la nature de vos relations avec lui, préparez plus ou moins de questions plus ou moins ciblées pour avoir la plus grande efficacité en un temps limité. Il est important de respecter les dates limites. Construire un dossier prend du temps, il est difficile de construire un bon dossier en moins d’un mois.

Il faut ensuite déposer son dossier. Généralement, les procédures à cet effet sont assez simples, souvent un envoi par mail. Penser à transmettre le dossier définitif aux contacts qui ont aidé à le construire, ne serait-ce que pour les informer.

### Suivre son dossier

Il est important d’assurer un certain suivi de son dossier. Immédiatement après la transmission du dossier, il faut s’enquérir de deux points importants du suivi : par quel moyen et quand sera transmise la réponse ?

Si des contacts sont présents parmi les circuits de décision, il est utile de leur rappeler les éléments clé du dossier quelques jours avant la prise de décision afin qu’il ait en main toutes les informations utiles.

Si la décision n’est pas notifiée au moment prévu, il peut être utile de contacter la personne en charge de l’administration des demandes pour prendre des nouvelles, vérifier s’il ne manque pas de pièce.
