# Logistique d'un tournoi local

## Matériel et équipement

Pour l’organisation d’un tournoi local, la notion de matériel et d’investissement est assez limitée. Avec le caractère annuel du tournoi, il ne serait que peu pertinent d’acquérir du matériel à ses fins. En revanche, il est assez utile de se faire prêter un certain nombre de matériels (par des organismes ou d’autres associations locales) :

 - un caddie (très utile pour le transport des différents goûters et autres produits).
 - une bouilloire.
 - une cafetière.
 - une trousse de premiers secours.

## Impressions

Un des points qui pose le plus souvent question est la gestion des impressions papier. Si la quantité de documents à imprimer a été drastiquement réduite depuis les débuts du tournoi, il reste toujours un ensemble de photocopies et d’impressions indispensables.

 - les documents imprimables avant même le tirage (règlement, énoncés, notes de synthèse vierges).
 - les documents imprimables dès le tirage au sort (fiches de notation, solutions écrites).
 - les documents imprimables juste avant les tours (notes de synthèse).

Pour un tournoi régional, en s’organisant correctement, tout peut être imprimé la semaine précédant le tournoi, à l’exception des notes de synthèse des équipes. Pour le premier tour la fenêtre est assez large (rendu le vendredi soir pour utilisation le samedi midi), pour le second la question est différente (généralement, seules 2 ou 3 heures séparent le rendu de l’utilisation des notes de synthèse).

> Chaque tournoi régional a donc besoin d’une solution d’impression en amont du tournoi et d’une solution de photocopie pendant le tournoi. Pour la finale nationale, une solution d’impression est nécessaire en amont et pendant le tournoi.

## Nourriture

### Goûters

Pour les goûters, on peut se référer à une structure classique de poule à 3, il y a 2 goûters chaque jour. On peut dégager les quantités suivantes :

Aliment                                   | Quantité par jour et par personne
-----------------------------------------:|----------------------------------
Gâteaux                                   | 200g
Fruits (les pommes sont une valeur sure)  | 200g
Boisson (hors eau)                        | 1L
Eau                                       | 0,25L
Café                                      | 1/4 unité
Thé (éviter le lipton yellow)             | 1/4 unité
Verres en plastiques                      | 4 unités

Privilégiez ce qui n’a pas besoin d’être conservé au frais et les emballages en grandes quantités,
la gestion des stocks en est d’autant simplifiée, et l'environnement vous en sera reconnaissant.

Pensez à répartir les ressources au préalable, affecter un stock à chaque goûter (donc 4 parties) pour éviter la surconsommation.

### Déjeuner du samedi

Pour des raisons pratiques d’organisation et aussi financières, le déjeuner du samedi se fait généralement avec une solution de type sandwich. L’intérêt pratique est essentiellement dû à la disjonction entre les équipes et l’arrivée du jury qui rend difficile un repas que l’on devrait prendre de manière synchrone.

> Ne pas oublier les contraintes alimentaires. À cet âge de participant il n’y a pas d’enjeu sur le fait de les surveiller, ils savent ce qu’ils peuvent manger ou non. En revanche, il faut prévoir de satisfaire les différents régimes alimentaires. Le plus simple est de prévoir une solution « sans produits animaux » qui généralement satisfait à tous les régimes particuliers.

Généralement, il est simple de compter 0,5L d’eau par personne par repas.

### Dîner du samedi

Le dîner est toujours un repas chaud. Les participants ont passé une dure journée, il est important qu’ils puissent se retrouver tous ensemble, avec les encadrants et organisateurs. La solution basique réside dans les pizzas. Chaque organisateur est invité à avoir de meilleures idées, plus variées !

> Ne pas oublier les contraintes alimentaires. À cet âge de participant il n’y a pas d’enjeu sur le fait de les surveiller, ils savent ce qu’ils peuvent manger ou non. En revanche, il faut prévoir de satisfaire les différents régimes alimentaires. Le plus simple est de prévoir une solution « sans produits animaux » qui généralement satisfait à tous les régimes particuliers.

Généralement, il est simple de compter 0,5L d’eau par personne par repas.

### Déjeuner du dimanche

Selon le planning choisi du tournoi, le déjeuner a lieu entre le deuxième et le troisième tour, ou bien après les tours. Dans le second cas, plus de liberté est possible. Le repas est froid, avec la variabilité de l’heure du repas due aux aléas des tours, c’est inévitable.

> Ne pas oublier les contraintes alimentaires. À cet âge de participant il n’y a pas d’enjeu sur le fait de les surveiller, ils savent ce qu’ils peuvent manger ou non. En revanche, il faut prévoir de satisfaire les différents régimes alimentaires. Le plus simple est de prévoir une solution « sans produits animaux » qui généralement satisfait à tous les régimes particuliers.

Généralement, il est simple de compter 0,5L d’eau par personne par repas.

### Goûter de fin de tournoi

En fin de tournoi, un goûter clôture l’événement. C’est l’occasion de vider les stocks et de donner un dernier moment de partage aux participants avant leur retour.

