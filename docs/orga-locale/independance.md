# Choisir son régime d'indépendance

Chaque tournoi local possède une organisation propre.
En ce qui concerne la gestion administrative et financière,
les cadres varient d’une région à l’autre mais trois systèmes se dégagent.
Avant toute réflexion, il convient de poser quelques principes des tournois locaux :

 - chaque tournoi local doit avoir ses sources de financement et un budget équilibré,
    quel que soit son régime d’indépendance.
 - les comités locaux d’organisation bénéficient de la même marge de décision
    quel que soit le régime d’indépendance.
 - le partenaire local principal bénéficie de la même communication,
    qu’il gère ou non directement les aspects administratifs et financiers.
 - la gestion des paiements des inscriptions fait généralement l’objet d’une procédure
    différente car simplifiée par des outils en ligne.


## Indépendance administrative et financière
</-- ### Description -->

Pour ce régime, le tournoi doit être porté par une personne morale locale
(le plus souvent, un établissement d’enseignement ou une association).
C’est cette personne morale qui va se charger de :

 - contracter des accords (généralement des conventions, parfois moins formel) avec des partenaires.
 - recouvrir des paiements desdits partenaires.
 - contracter des engagements (devis, bons de commande, parfois moins formel) envers les fournisseurs.
 - effectuer les paiement desdits fournisseurs.



En ce qui concerne les liens juridiques, Animath et l’organisme organisateur signeront
une convention d’organisation, et ce sera ensuite à l’organisme organisateur de créer
ses propres liens avec les partenaires et prestataires


!!! success "Avantages"

    Une gestion locale permet une certaine proximité géographique entre les décisionnaires
    locaux et l’administration / comptabilité peut se révéler utile.
    D’autre part, et il ne faut pas négliger ce point,
    certains organismes de financement ne passeront que par certains autres organismes,
    ou bien exigeront de prendre directement en charge des factures. Ces points sont à analyser.


!!! warning "Inconvénients"

    Si l’organisme local est une association, il n’y a pas fondamentalement d’inconvénient.
    Il s’agit surtout d’analyser la situation au cas par cas.
    Le point qui est le plus souvent en question dans ces cas est l’avance de trésorerie.
    Il est d’usage que l’on paie les fournisseurs avant les prestations (sauf exception)
    et que les subventions ne soient versées qu’après le tournoi.

    Si l’organisme est une entité publique, les choses peuvent se compliquer.
    En cause le code des marchés publics qui demandera dans certains cas un petit appel d’offre,
    dans d’autres vous imposera un fournisseur (qui a précédemment gagné un appel d’offre).
    Il faut éclaircir ce point précis avec l’institution envisagée avant de s’engager sur ce point.


## Indépendance administrative, intégration financière
### Description

Dans ce régime, le principe est de s’appuyer sur une institution locale
qui va se charger de toute l’administration de premier ordre et d’utiliser Animath
comme responsable pour tout ce qui est financier.

Si l’on reprend la structure en quatre tâches :

 - contracter des accords (généralement des conventions, parfois moins formel) avec des partenaires.
 - recouvrir des paiements desdits partenaires.
 - contracter des engagements (devis, bons de commande, parfois moins formel) envers les fournisseurs.
 - effectuer les paiement desdits fournisseurs.

Animath assume les contrats formels et la gestion des paiements.
L’organisme local assume les engagements moins formels et prépare les formalités
pour qu’Animath soit en contact avec les fournisseurs et édite les conventions.

En ce qui concerne les dépenses, pour donner plus de flexibilité, deux systèmes cohabitent :

 - pour les petites sommes, les organisateurs avancent la dépense
    qui leur est ensuite remboursée par note de frais.
 - pour les dépenses plus importantes, l’association paie les fournisseurs par virement.



Le rôle juridique de l’organisme local n’est pas réellement existant,
il s’agit plutôt d’une facilité offerte localement. Toutes les conventions seront signées
entre Animath et les partenaires. Il n’y a pas d’organisme organisateur local
identifié juridiquement, l’organisation qui accueille le tournoi sera liée par une convention
d’occupation des locaux et/ou de financement.


### Avantages

Avec une telle organisation, les organisateurs locaux ont une certaine proximité avec la gestion des affaires courantes ce qui facilite la gestion au quotidien. Généralement, le partenaire investi de la mission administrative participe de cette manière au tournoi (et cette aide est précieuse), souvent car il n’a pas nécessairement de grand budget à allouer en versement pour le tournoi.

Il faut aussi notes que ceci libère des questions de marché public et de trésorerie qui peuvent se poser pour le régime précédent.


### Inconvénients

Ce régime est beaucoup basé sur un « flou artistique » en terme de responsabilités qui se gère très bien avec des partenaires de longue date. Cependant, il peut être délicat de commencer sur ce principe. L’enjeu est essentiellement de se fixer une limite claire entre les domaines de compétence de chacun. Cette organisation est donc assez complexe à mettre en place bien que très efficace une fois fonctionnelle


## Intégration administrative et financière
### Description

Le régime d’intégration permet de gérer l’intégralité de l’administration et
de la comptabilité en interne à Animath. L’association se charge donc de :

 - contracter des accords (généralement des conventions, parfois moins formel) avec des partenaires ;
 - recouvrir des paiements desdits partenaires ;
 - contracter des engagements (devis, bons de commande, parfois moins formel) envers les fournisseurs ;
 - effectuer les paiement desdits fournisseurs.

Généralement, le Comité local d’organisation rassemble les documents nécessaires
puis les envoie et ils sont traités au siège.

En ce qui concerne les dépenses, pour donner plus de flexibilité, deux systèmes cohabitent :

 - pour les petites sommes, les organisateurs avancent la dépense
    qui leur est ensuite remboursée par note de frais.
 - pour les dépenses plus importantes, l’association paie les fournisseurs par virement.



Il n’y a pas d’organisme local identifié, l’organisation qui accueille le tournoi
sera liée par une convention d’occupation des locaux et/ou de financement.


### Avantages

Cette méthode permet de ne pas avoir besoin d’une institution locale,
car dans certains cas, il n’existe que des administrations à organisation peu adaptée
aux nombreuses petites dépenses du tournoi. Les organisateurs ont généralement un contrôle
plein et entier sur les processus qui sont ensuite validés par Animath.


### Inconvénients

Ce système prend un peu plus de temps aux organisateurs que le précédent.
Il peut arriver que certains organismes de financement aient des conditions de financement
locales et refusent de participer financièrement si les sommes d’argent doivent passer par
une comptabilité « lointaine », ou plus couramment,
certains organismes ne veulent pas contribuer à un budget mais veulent prendre à
leur charge une facture
(encore une fois, le code des marchés publics se cache derrière cette contrainte).


## Comment choisir ?

Le régime d’indépendance n’est pas le choix le plus simple à faire.
Ce choix peut réduire l’éventail des possibles pour les financements
et il faut y prendre garde. Aussi, s’il est intéressant de réfléchir à ce choix
dès les prémices de l’organisation. Mais il n’est pas conseillé de s’y contraindre
avant d’avoir la première convention d’un autre partenaire.

