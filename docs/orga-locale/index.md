# Tournois régionaux et finale

Les tournois locaux sont la partie que les participants voient du tournoi,
celle qui est à la base de tout avec les sujets. 
D’autre part, les tournois régionaux se développent énormément depuis leur création en 2015.

Les [bases d’un tournoi local](les-bases.md) vous permettront d’entrer dans le sujet
de manière assez synthétique pour aborder les différents enjeux.

Les [ressources documentaires](ressources-doc.md) locales rassemblent tous les documents utiles au tournoi :
de l’autorisation de prises de vues à la fiche de notes du jury en passant 
par des email types à envoyer aux participants ou des feuilles de calcul de budget.

Les [ressources de formation](before) locales vous permettront d’apprendre
un certain nombre de méthodes utilisées dans les tournois pour gérer
les différentes missions d’organisation. 
Elles permettent aussi de se tenir à jour sur un certain nombre de sujets
qui évoluent comme la gestion des bases de données, les assurances,
la place du bénévole au sein de l’association par exemple.

