# Les lieux du tournoi

Chaque tournoi possède ses spécificités locales, ainsi, plusieurs lieux évoqués ci-dessous
pourront être les mêmes selon les tournoi.

!!! tip "Conseil"
    Il est conseillé de fixer les lieux dans l’ordre où ils sont cités ici,
    du plus important au plus ajustable.
    Bien sûr, les configuration de lieux qui sont évoquées plus bas n’existent jamais,
    il est donc plus important d’en être raisonnablement proche que de chercher
    désespérément à la satisfaire toutes.

## Partie tournoi

### Salles pour les présentations des équipes

Pour que le tournoi ait lieu, il faut des salles pour permettre aux équipes de présenter leurs travaux. Les caractéristiques de telles salles :

 - autant de salles que de poules.
 - au moins 40 places par salle.
 - vidéo-projecteur utilisable facilement (éviter tant que possible les amphis avec régie).
 - surface de tableau accessible en même temps que le projecteur est utilisé (et craies / feutres associés).
 - les spectateurs doivent avoir des tables (éviter les auditoriums).
 - ne pas être trop grandes (l’impression de « vide » est mauvaise pour les élèves).
 - ne pas nécessiter l’utilisation d’un micro.

### Espace pour le goûter

Entre deux tours de présentation et de débat, un goûter est organisé.
Il est donc important qu’un espace proche des lieux des présentations soit utilisable pour y disposer un goûter.
Les caractéristiques d’un tel espace :

 - suffisamment d’espace pour que tous les participants y soient simultanément.
 - 4 ou 5 tables.

### Salles de travail

Entre la fin du premier tour et le début du second, les équipes travaillent dans des salles qui leur sont dédiées.
Les caractéristiques de telles salles :

 - autant de salles que d’équipes
 - la taille des salles n’a pas d’importance
 - vidéo-projecteur utilisable par les équipes
 - tableau(x) et craies / feutres

Il est primordial que ces salles puissent être accessibles par les équipes après le dîner du samedi.
Usuellement, les équipes souhaitent rester jusqu’à minuit, voire 1h.
Il est souhaitable que les salles soient un maximum accessibles pour la soirée.

### Salle des organisateurs

Pendant les 48h que dure le tournoi, les organisateurs ont besoin d’une salle qui leur est propre.
Elle doit être proche du lieu du goûter.
Cette salle des organisateurs sert généralement à la fois pour le stockage et comme base pour l’équipe d’organisation.
Il n’y a pas de contraintes spécifiques sur la salle en question, on pourrait les résumer comme suit :

 - plus de 15m².
 - plus de 10 chaises (tables associées).
 - privilégier les rez-de-chaussée.

## Partie hébergement

### Hébergement

Les équipes doivent être hébergées du samedi au dimanche. Deux cas sont à disjoindre.
D’une part les encadrants, pour lesquels il faut prévoir un hébergement relativement confortable
(hôtel, résidence étudiante). D’autre part, les participants qui eux peuvent être logés
avec une solution à bas coût comme par exemple :

 - hôtel (à 2 ou 3 par chambre).
 - auberge de jeunesse (chambre multiple).
 - gymnase / salle de sport organisé en dortoirs.

Les coûts des différentes possibilités peuvent être sensiblement différents.
Il ne faut pas oublier de prendre en compte l’éloignement aux autres lieux du tournoi en choisissant l’hébergement.

### Espace pour le petit déjeuner

Le petit déjeuner peut être inclus dans l’hébergement ou non. S’il faut organiser un petit déjeuner, il faut trouver un lieu qui s’y prête. Généralement, les prestataires d’hébergement proposent le petit déjeuner. Si cette prestation est à prix raisonnable, il est préférable d’y avoir recours, ceci simplifie l’organisation.

## Partie repas

### Espace pour les déjeuners

Un espace doit être prévu pour les déjeuners. Les contraintes sur cet espace sont essentiellement les suivantes :

 - pouvoir accueillir l’ensemble des participants, encadrants, membres du jury et organisateur;
 - de préférence avoir une place assise pour chacun (dépend du type de déjeuner).
 - avoir un espace de « service ».

!!! todo
    Ces aspects sont développés plus précisément dans la page ZZZ

### Dîner

Un espace doit être prévu pour le dîner. Les contraintes sur cet espace sont essentiellement les suivantes :

 - pouvoir accueillir l’ensemble des participants, encadrants, membres du jury et organisateurs.
 - avoir une place assise pour chacun.
 - avoir un espace de « service ».


!!! todo
    Ces aspects sont développés plus précisément dans la page ZZZ

## Accueil

Il est nécessaire d’organiser le premier jour un espace d’accueil, point de rendez-vous pour tous les participants. Le point d’accueil fait l’arbitrage entre deux caractéristiques :

 - être proche des autres lieux du tournoi.
 - être le plus facile à trouver possible pour les équipes.

