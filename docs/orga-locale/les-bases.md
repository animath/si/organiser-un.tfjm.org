# Les bases d'un tournoi local

Lors de la création ou de la vie d’un tournoi local,
qu’il s’agisse d’une édition régionale ou de la finale nationale,
différents pointe apparaissent comme incontournables et sont ainsi
les clés d’un tournoi efficace. Cette page recense les principaux enjeux.


## Le Comité local d’organisation

Le tournoi repose avant tout sur les femmes et les hommes qui donnent
de leur temps pour l’organiser. Le Comité local est donc le centre de gravité
du tournoi et doit le rester.

Les responsabilités du Comité local couvrent tout ce qui concerne le tournoi
dans sa particularité locale, c’est-à-dire :

 - les locaux pour la partie pédagogique.
 - l’hébergement et la restauration des participants.
 - le financement du tournoi local.
 - la définition précise du planning.
 - la gestion de l’inscription des participants (grâce à la plateforme dédiée).
 - le recrutement du jury.
 - des mesures de communication vers les participants.
 - la gestion de certains éléments pédagogiques.


!!! todo
    [Plus d’informations sur le Comité local d’organisation](https://www.organiser-un.tfjm.org/ressources-de-formation/ressources-humaines/recrutement-du-comite-local-dorganisation/)


## Articulation avec le Comité national d’organisation

Si le Comité local assume la responsabilité sur ces points, il n’est pas
seul pour ces missions. Le Comité national d’organisation fournit aux comités locaux
un certain nombre de fonctions support, on peut citer par exemple :

 - la mise en contact avec de potentiels bénévoles parmi les anciens participants.
 - l’aide à la création de dossiers de financement.
 - des plannings caractéristiques.
 - la gestion de la plateforme et des paiements des inscriptions.
 - des listes des membres du jury précédents.
 - l’intégralité des éléments pédagogique pré-fabriqués.

En ce qui concerne la gestion du tournoi, le Comité national d’organisation
fournit une aide qui dépend de l’expérience des organisateurs régionaux.
Si un tout nouveau tournoi avec une équipe nouvellement créée a usuellement
besoin d’un fort soutien ; les tournois où les équipes sont stables et avec
plusieurs années d’expérience sont beaucoup plus autonomes.

Les tournois sont encouragés à trouver le bon niveau d’autonomie et à le faire progresser chaque année. Plus les tournois expérimentés sont autonomes, plus le comité national dispose de temps et de ressources pour soutenir ceux qui débutent.

!!! info ""
    [Plus d’informations sur la coordination nationale](../../orga-nationale)


## L’institution support

Chaque tournoi repose sur une institution : université, école ou lycée.
Il s’agit généralement de l’établissement qui accueille physiquement
les équipes pour les débats. Cette institution doit être associée un
maximum au tournoi pour plusieurs raisons :

 - elle rendra le tournoi possible et le plus agréable possible pour tous.
 - elle délivrera probablement le premier financement.
 - elle vous permettra de convaincre plus facilement de nouveaux partenaires.

C’est aussi au travers de cette institution que l’on peut trouver des ressources
moins habituelles (mise à disposition de doctorants sur leurs heures de formation par exemple).

L’institution pourra aussi vous offrir une exposition en terme de communication qu’il
n’est pas évident de trouver ailleurs.

L'institution organisatrice à autant de formes qu'il existe de tournois régionaux,
mais on peut cependant distinguer trois modèles :

 - **autonomie complète** : une institution locale est chargée de l’organisation
    du tournoi local, assure son financement, sa comptabilité, et est liée à Animath
    par un cadre contractuel (convention d’organisation).
 - **forte autonomie** : une institution locale est chargée de l’organisation du tournoi local,
    assure son financement, dans un dialogue avec Animath et y est liée par un cadre contractuel
    (convention d’occupation et de financement avec des articles sur l’organisation).
 - **faible autonomie** : ce sont plus les bénévoles que l’institution qui sont à la manœuvre,
    toute l’administration est gérée à Animath, le lien contractuel est moins engageant
    (convention d’occupation d’espaces et de financement).

!!! info ""
    [Plus d’informations sur les modèles d'indépendance](../before/independance)


## Les lieux du tournoi

Le tournoi gravite autour de trois lieux : mathématiques, restauration, hébergement.
Pour que l’organisation du tournoi soit la plus facile possible,
il faut que ces trois lieux soient les plus proches possibles.

Il faut aussi prendre en compte des aspects plus en lien avec l’accès au site 
(mais ceci est moins important que la recommandation précédente), 
plus le lieux du tournoi est accessible, plus les participants sont à l’heure le jour de l’arrivée.
Il faut au moins être attentif à ce point pour l’accueil du premier jour.



!!! todo
    [Plus d’informations sur les lieux du tournoi](https://www.organiser-un.tfjm.org/ressources-de-formation/lieux-du-tournoi/)

 
## Les partenaires

Les partenaires sont souvent en premier lieu des financeurs du tournoi.
Mais leur apport ne se limite pas à ce côté financier. La plupart peuvent aussi mettre 
à disposition des locaux ou du matériel. Mais ils seront aussi très utiles pour
former le jury de l’édition locale, ou bien avoir un système de communication efficace,
des lots pour les participants et autres ressources pour lesquelles on ne les attendrait pas.

Il est important de nouer avec les partenaires une relation de confiance sur le long terme.
Il faut les informer régulièrement des avancées du tournoi, ne pas hésiter à les solliciter
lorsque le tournoi se trouve face à un obstacle qu’ils peuvent plus facilement lever
(veiller à leur demander de l’aide sur des points précis et clairement identifiés
afin que l’action pour eux soit la plus simple possible).


!!! todo
    [Plus d’informations sur les partenaires](https://www.organiser-un.tfjm.org/ressources-de-formation/partenariats/)

 
## Le jury

Le jury des éditions locales est recruté localement.
Chaque poule (groupe de 3 équipes) possède un jury qui se compose de :

 - un président de jury (recruté par le comité national)
 - entre trois et sept membres du jury (recrutés par le comité local)

Une attention toute particulière doit être portée au jury.
Ce sont ces enseignants et chercheurs qui permettent la solidité scientifique du tournoi.

Le jury se recrute bien en amont du tournoi. Cette opération est à mener de concert
avec le comité national pour plus de facilité et pour éviter que certains chercheurs
soient contactés par de multiples régions.

!!! todo
    [Plus d’informations sur le jury](https://www.organiser-un.tfjm.org/ressources-de-formation/les-jurys/)


## Les équipes

Bien sûr, la concrétisation du tournoi, ce sont les équipes de participants.
On peut distinguer trois phases en ce qui concerne les équipes.

#### Avant l’inscription

Il s’agit d’une phase de communication qui vise à attirer les participants.
L’enjeu est essentiellement de faire connaître le tournoi et d’aider dans certains cas
les jeunes à trouver un encadrant dans le cas d’équipes spontanées.
Lors de cette phase, il s’agit de communication vers le grand public.

#### Entre l’inscription et le tournoi

Lors de cette phase, la communication est d’ordre pratique : 
détails sur l’organisation, programme, lieux sont autant d’éléments qui,
s’ils sont clairs avant l’arrivée des participants sur place seront autant de problèmes
de moins à gérer pendant le tournoi.

#### Pendant le tournoi

De nombreux messages sont utiles à délivrer aux participants pendant le tournoi,
au premier rang desquels une sensibilisation sur le fait que les organisateurs
sont bénévoles et qu’eux aussi, dans quelques mois ou quelques années pourront rejoindre
l’équipe et permettre au tournoi de grandir encore !

!!! todo
    [Plus d’informations sur la communication](https://www.organiser-un.tfjm.org/ressources-de-formation/communication/)


## La logistique

Lors du tournoi, ce sont plusieurs de jeunes et un certain nombre d’encadrants
qu’il faudra accueillir. Les gouters organisés entre les passages des équipes
sont un moment charnière du tournoi. Généralement, les déjeuners et le dîner du samedi soir
sont à organiser (sauf dans quelques cas où le prestataire d’hébergement les fournit).
Il est donc nécessaire de faire des courses, de trouver des prestataires et/ou des fournisseurs.
Pour ce faire, quelques éléments d’expérience des autres tournois peut être très utile.

!!! todo
    [Plus d’informations sur la logistique](https://www.organiser-un.tfjm.org/ressources-de-formation/logistique-dun-tournoi-local/)

