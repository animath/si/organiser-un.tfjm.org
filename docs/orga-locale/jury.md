# Le jury

Le jury est la caution scientifique du tournoi,
c’est donc un élément de première importance dans le montage de l’édition locale du tournoi.
Chaque poule a un jury chaque jour. Le jury du premier jour est indépendant
de celui du second. Il y a autant de jury que de poules.

## Composition du jury

Un jury se compose de 4 à 8 membres, dont un président de jury.
On peut dégager une structure « idéale » de jury comme suit :

 - 1 président de jury
 - 1 ancien participant entre bac+1 et bac+3
 - 1 étudiant bac+4 ou bac+5 (de préférence ancien participant)
 - 1 enseignant du secondaire ou de classes préparatoires
 - 1 doctorant
 - 1 enseignant chercheur

Bien sûr cette structure est faire pour un jury à 6
(ce qui est courant dans les tournois régionaux) et il n’est pas problématique
de s’en éloigner ; mais il est important de la garder à l’esprit comme objectif.

## Accueil du jury

Comme les tours ont lieu juste après le repas et même souvent à cheval sur le repas du midi,
il est d’usage de proposer aux membres du jury de déjeuner avec les participants,
encadrants et organisateurs le midi. L’autre avantage de cette invitation est
de se permettre une plus grande sécurité de planning : les membres ne sont pas en retard
puisqu’ils sont venus en avance pour déjeuner, et si l’organisation est légèrement en retard,
ils savoureront un peu plus longtemps leur dessert.

Accueillir le jury c’est aussi leur remettre avant chaque tour un sac avec
tout le matériel permettant la notation (attention : le président de jury a quelques
documents en plus). Ce aspects sont plus développés dans les documents du tournoi.

## Recrutement du jury

### Procédure de recrutement

L’action de recrutement des membres peut être menée totalement en autonomie,
ou bien en lien avec le Comité national d’organisation. Autant que possible,
il faut privilégier la procédure liée au CNO car :

 - elle permet d’avoir un questionnaire d’inscription simple et déjà fait
    (moins de travail et moins de risque en local)
 - elle permet d’éviter de contacter deux fois, par des canaux différents, les mêmes personnes.

La procédure concertée se résume comme suit :

 - le comité local communique au comité national les informations pratiques sur le tournoi
    et ce qui est envisagé comme diffusion de l’appel à candidature.
 - le comité national créé un questionnaire, permet au comité local de l’administrer,
    identifie des contacts en doublons (car présents dans plusieurs régions)
    et propose des contacts supplémentaires.
 - le comité local diffuse l’appel à candidature pour les potentiels membres.

### Diffusion de l’appel à candidature

Pour le recrutement du jury, on peut distinguer plusieurs types de membres,
et donc plusieurs canaux pour les atteindre.

#### Enseignants chercheurs et doctorants

Grâce à leur rassemblement au sein des laboratoires, il est assez simple de toucher
massivement ce public. L’essentiel de l’action consiste à trouver un contact dans chaque
laboratoire qui sera prêt à relayer un message d’invitation à ses collègues.
Il faut veiller à trouver ce contact et à lui fournir les informations suffisamment
en amont pour qu’il puisse le faire tranquillement.

#### Enseignants du secondaire et classes préparatoires

Le réseau des anciens encadrants, voire des anciens participants saura fournir
ce type de contacts. Il s’agit plus d’un recrutement « à la personne » que d’une diffusion
massive. Il est donc d’usage de prendre contact avec la personne
(le plus souvent via un intermédiaire) puis par la suite de lui envoyer de quoi s’inscrire.
Le Comité national d’organisation possède souvent plus de contacts que les régions
grâce à des informations qui remontent plus loin dans le temps
et qui concernent toutes les régions.

#### Étudiants et anciens participants

Dans ce cas précis, le réseau des anciens participants est la clé du système.
Il faut à tout prix éviter l’envoi massif à tous les particpants de l’année précédente et
se concentrer sur des éléments singuliers : finalistes, élèves s’étant montrés
particulièrement investis et curieux alors qu’ils étaient participants, etc.
Dans ce cas aussi, le comité national peut avoir des informations supplémentaires.

!!! warning "N’oubliez jamais les organisateurs !"

    Assez souvent oubliés lors du recrutement du jury,
    les organisateurs n’en sont pas moins d’excellents membres.
    Ne restez pas sur le « on aura autre chose à faire pendant le tour » :
    un (maximum deux) organisateur suffit à organiser le goûter, faites un roulement
    entre les deux jours du tournoi. Il est important que les organisateurs puissent
    participer au jury : voir les élèves présenter leurs travaux, c’est mesurer tout
    son apport dans le tournoi.

### Calendrier de communication vers les membres du jury

 - **janvier :** écrire aux membres du jury des années précédentes et aux personnes
    avec qui les organisateurs sont en interface directe pour les informer de la date
    du tournoi et leur mentionner qu’il y aura besoin d’un jury et préciser en quoi cela consiste
 - **mars (6 semaines avant le tournoi) :** diffuser l’appel à candidature par un email
    contenant un lien vers le formulaire pour s’inscrire ; veiller à ce que cet email
    soit facilement utilisable par un contact relai (utiliser au maximum des formes
    impersonnelles, bien faire figurer toutes les informations)
 - **2 semaines avant le tournoi :** confirmer aux inscrits les jours et heures auxquels
    ils sont attendus (et mentionner le déjeuner pour rappel), leur communiquer un contact
    téléphonique
 - **en début de semaine avant le tournoi :** avec l’envoi des solutions écrites des équipes,
    rappeler les informations pratiques et le contact téléphonique

## Président de jury

Les présidents de jury sont nommés par le Comité scientifique. Bien sûr,
les questions de disponibilité et de distance sont prises en compte.
Cette affectation est connue largement à l’avance (au moins 1 mois)
et doit donc être prise comme une entrée de l’algorithme d’organisation.

Selon les situations, les présidents de jury peuvent avoir besoin d’être hébergés.
Dans ce cas, prévoir la même solution que pour les encadrants
(pour le dîner et l’hébergement) facilite l’organisation.i

