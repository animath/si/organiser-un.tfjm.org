# Organiser un TFJM²

Bienvenue sur le site des organisateurs du TFJM² !
Ce site web traite des sujets d’organisation du tournoi et vous aidera dans cette mission. 

!!! info "Audience"

    Si vous n’êtes ni organisateur, ni futur organisateur, le site que vous cherchez est [tfjm.org](https://tfjm.org).


## Que contient organiser-un.tfjm.org ?

Ce site web contient trois types d’éléments :

 - des informations sur le tournoi, son fonctionnement interne,
    la façon dont s’organisent les choses, la responsabilité de chacun.
 - des documents pour l’organisation du tournoi afin que les organisateurs
    puissent les trouver le plus facilement possible.
 - des ressources de formation sur une grande partie des aspects du tournoi
    afin de rendre les choses le plus simples possibles aux nouveaux organisateurs
    (et aussi d’apporter du renouveau aux plus chevronnés).


## Pourquoi utiliser organiser-un.tfjm.org ?

Un site web, c’est avant tout un contenu et une forme modulable et accessible à tous.
Ce site web est conçu pour les organisateurs débutants et ils sont donc son principal public.
Mais les organisateurs plus expérimentés ne doivent pas le délaisser.
D’une part car ce site contient nombre de documents dont ils ont besoin et qui sont renouvelés,
d’autre part parce que les pages de formation leur sont aussi destinées !


## Comment contribuer à organiser-un.tfjm.org ?

Chacun peut facilement contribuer au site web.
Chaque tournoi peut avoir des conseils à partager avec les autres organisateurs, présents ou futurs.
Plus il y aura de contenu, plus les passations, les créations de tournoi,
les nouveaux organisateurs pourront construire sur ce qui existe au lieu de le réinventer.
Ce partage d’informations est la clé vers une organisation toujours plus efficace.

Aussi, si vous avez la possibilité ou l'envie d’améliorer ce site web,
de proposer des conseils aux autres organisateurs, de partager des documents
qui vous aident à organiser le tournoi dans votre région, n’hésitez pas.

Pour le faire, il y a deux moyens :

 - tout ce site est contenu dans des fichiers *Markdown* dans [un dépot Gitlab](https://gitlab.com/animath/si/organiser-un.tfjm.org)
    auquel vous pouvez contribuer avec le système d'issues et de MR.
    Plus d'information sur [Contribuer à ce site](contributing.md)
 - ou informer le Comité national d’organisation 
    (via vos contacts habituels, ou par mail à <contact@tfjm.org>).

