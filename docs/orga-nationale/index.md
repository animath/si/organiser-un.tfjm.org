# La coordination nationale

Pour tous les aspects qui sortent de l’organisation pratique des tournois,
la gestion est coordonnée à l’échelle nationale.
Les deux principales missions de la coordination nationale sont la qualité pédagogique
et scientifique du tournoi d’une part, 
et le bon fonctionnement de l’organisation d’autre part.

Ces missions donnent lieu à deux comités :


## Comité scientifique

Le Comité scientifique est en charge de toute la partie pédagogique du tournoi, c’est-à-dire :

 - la rédaction des sujets.
 - la rédaction (en concertation avec le CNO) du règlement.
 - la gestion des jurys et en particulier la nomination des présidents de jury.
 - la gestion des procédures d’évaluation (barèmes).
 - la gestion des notes et des qualifications.


## Comité national d’organisation

Le Comité national d’organisation est en charge de l’organisation du tournoi,
tant sur des aspects pratiques que sur la coordination entre les différents tournois locaux.
Sa mission est de mener à bien la direction du tournoi et donc en particulier,
d’assister les organisateurs locaux dans leurs difficultés.

Ses responsabilités comprennent :

 - la rédaction (en concertation avec le Comité scientifique) du règlement.
 - l’établissement (en concertation avec les autres comités) du calendrier du tournoi.
 - la gestion des services communs (site web, plateforme d’inscription).
 - le soutien aux comités locaux selon leurs besoins.
 - l’appui pour la recherche de nouveaux partenaires.
 - la communication du tournoi.
 - la gestion des finances nationales.
 - la gestion (en concertation avec les comités locaux et selon les régimes d’indépendance) des finances locales.
 - la gestion administrative et comptable du tournoi (conventions, devis, factures, selon les régimes d’indépendance).

!!! todo
    Fusionner cette page là et [les bases](les-bases.md) ?
    Créer deux pages, une pour le comité problème, une pour le CNO ?
