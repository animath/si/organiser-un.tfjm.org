# Les bases de la coordination nationale

## Le Comité national d’organisation

Le CNO est le point de convergence des informations,
donc par essence le point de convergence des problèmes.
Le Comité est donc en charge de la résolution de nombreux problèmes imprévus au fil de l’année.
C’est aussi le CNO qui se charge de l’exécution du calendrier
et qui veille à ce que tout ce qui était prévu soit réalisé.

Le CNO est également l’instance de coordination entre les régions,
c’est-à-dire celle qui permet de partager les bonnes initiatives locales
à toutes les régions pour que le tournoi soit plus facile à organiser.
En cela il faut toujours pour les organisateurs nationaux être à l’écoute des pratiques
et des innovations dans les tournois régionaux pour pouvoir partager les meilleures.

!!! todo
    [Plus d’informations sur le rôle du Comité national d’organisation.](Ahah)


Le Comité national compte entre 4 et 8 membres (en général).
Les missions sont assez diverses mais elles demandent une grande coordination au sein de l’équipe.
Les membres du CNO sont bénévoles, généralement étudiants.

!!! todo
    [Plus d’informations sur le recrutement du Comité national d’organisation.](Ahah)


## Le Comité scientifique

Le Comité scientifique assume une double mission :

 - la conception des sujets.


## Articulation avec les tournois régionaux et la finale

!!! todo



## Constituer les équipes

!!! todo



## Fonctionnement avec la direction d’Animath

!!! todo

