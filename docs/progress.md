# Progrès

Cette page reporte le progrès de la migration de [l'ancien site](www.organiser-un.tfjm.org) vers le nouveau.


#### Le but du projet

Le but de ce projet est de faire en sorte qu'au fil des années, le TFJM² puisse 
profiter un maximum de ce qui a déjà été fait, et puisse construire plus facilement par dessus
au lieu de passer du temps à reprendre les mêmes décisions et réinventer les mêmes choses.

Dans cette optique, ce site de passation vise à donner des bases aux différents bénévoles pour se
former dans ce qu'ils font et indiquer comment les choses sont généralement faites au sein du TFJM².

Ce site se propose donc d'être un repère en cas de doute et un guide pour accompagner la volonté de progresser.

#### Ce qui a déjà été fait

La majorité du contenu de l'ancien site à été migré (voir plan si dessous) et les 4 dernières pages seront 
transférées dans les prochains jours par Yohann et Diego. Les page sont toute à des endroits qui semble corrects,
mais on peut toujours réfléchir sur une structure un peu différente. En particulier, cette structure est
très différente de celle proposée par Martin.

#### Ce qu'on veut faire

L'idée est maintenant d'ajouter des pages ou de les compléter avec tout ce qu'on aurait bien
aimé avoir comme ressource cette année, soit parce que l'absence de cette ressource nous a géné
soit parce qu'elle aurait pu simplifier un vie et faire gagner en efficacité. Voici un liste non exaustive 
de choses sympa.

  - Ajouter pas mal de documents utiles (feuilles de notations pour jury, diplomes, template pour
    les problèmes ?, liste de taches pour les orga...).
  - ajouter des pages sur le role que chacun à eu, afin que le suivant ne se retrouve pas perdu (ex: un page respo info,
      une respo comm...).
  - n'importe quoi qui nous passerait par la tête d'utile.

Ça serait chouette d'actualiser cette liste en fonction de nos envie et de ce qu'on fait.

## Plan du site originel:

Les cases cochées indiquent que les pages ont été complètement transférées, et les cases vides qu'il y a encore du boulot.

* [x]   [Accueil](https://www.organiser-un.tfjm.org/)
* [x]   [Choisir son régime d’indépendance](https://www.organiser-un.tfjm.org/choisir-son-regime-dindependance/)
* [x]   [Informations légales](https://www.organiser-un.tfjm.org/informations-legales/)
* [x]   [Structure d’organisation](https://www.organiser-un.tfjm.org/structure-dorganisation/) {>>Presque vide<<}
    * [x]   [Les comités](https://www.organiser-un.tfjm.org/structure-dorganisation/comites/) {>>Vide<<}
    * [x]   [Association Animath](https://www.organiser-un.tfjm.org/structure-dorganisation/association-animath/)
    * [x]   [Organisateurs locaux et partenaires](https://www.organiser-un.tfjm.org/structure-dorganisation/organisateurs-locaux-et-partenaires/) {>>doublon<<}
* [x]   [Tournois régionaux et finale](https://www.organiser-un.tfjm.org/tournois-regionaux-et-finale/)
    * [x]   [Les bases d’un tournoi local](https://www.organiser-un.tfjm.org/tournois-regionaux-et-finale/bases-tournoi-local/)
    * [x]   [Ressources documentaires locales](https://www.organiser-un.tfjm.org/tournois-regionaux-et-finale/ressources-documentaires-locales/) {>>Penser à récuperer le pdf<<}
    * [ ]   [Ressources de formation locales](https://www.organiser-un.tfjm.org/tournois-regionaux-et-finale/ressources-de-formation-locales/)
* [x]   [Coordination nationale](https://www.organiser-un.tfjm.org/coordination-nationale/) {>>doublon<<}
    * [x]   [Les bases de la coordination nationale](https://www.organiser-un.tfjm.org/coordination-nationale/les-bases-de-la-coordination-nationale/)
    * [x]   [Ressources documentaires nationales](https://www.organiser-un.tfjm.org/coordination-nationale/ressources-documentaires-nationales/) {>>Vide<<}
    * [x]   [Ressources de formation nationales](https://www.organiser-un.tfjm.org/coordination-nationale/ressources-de-formation-nationales/) {>>Vide<<}
* [ ]   [Ressources de formation](https://www.organiser-un.tfjm.org/ressources-de-formation/)
    * [x]   [Administration et comptabilité](https://www.organiser-un.tfjm.org/ressources-de-formation/administration-et-comptabilite/) {>>Vide<<}
        * [x]   [Assurances et responsabilités](https://www.organiser-un.tfjm.org/ressources-de-formation/administration-et-comptabilite/assurances-et-responsabilites/) {>>Vide<<}
        * [x]   [Autorisations de participation, de prise de vue, fiche sanitaire](https://www.organiser-un.tfjm.org/ressources-de-formation/administration-et-comptabilite/autorisations-de-participation-de-prise-de-vue-fiche-sanitaire/) {>>Vide<<}
        * [ ]   [Établir un budget prévisionnel (local)](https://www.organiser-un.tfjm.org/ressources-de-formation/administration-et-comptabilite/etablir-un-budget-previsionnel-local/)
        * [ ]   [Procédures administratives de comptabilité](https://www.organiser-un.tfjm.org/ressources-de-formation/administration-et-comptabilite/procedures-administratives-de-comptabilite/)
        * [x]   [Règlement européen sur la protection des données](https://www.organiser-un.tfjm.org/ressources-de-formation/administration-et-comptabilite/reglement-europeen-sur-la-protection-des-donnees/) {>>Vide<<}
    * [x]   [Organisation pratique d’un tournoi](https://www.organiser-un.tfjm.org/ressources-de-formation/organisation-pratique-dun-tournoi/) {>>Vide<<}
        * [x]   [Les jurys](https://www.organiser-un.tfjm.org/ressources-de-formation/organisation-pratique-dun-tournoi/les-jurys/)
        * [x]   [Lieux du tournoi](https://www.organiser-un.tfjm.org/ressources-de-formation/organisation-pratique-dun-tournoi/lieux-du-tournoi/)
        * [x]   [Logistique d’un tournoi local](https://www.organiser-un.tfjm.org/ressources-de-formation/organisation-pratique-dun-tournoi/logistique-dun-tournoi-local/)
    * [x]   [Ressources humaines](https://www.organiser-un.tfjm.org/ressources-de-formation/ressources-humaines/) {>>Vide<<}
        * [x]   [Évaluation des ressources bénévoles](https://www.organiser-un.tfjm.org/ressources-de-formation/ressources-humaines/evaluation-des-ressources-benevoles/) {>>Vide<<}
        * [x]   [La place du bénévole dans l’association](https://www.organiser-un.tfjm.org/ressources-de-formation/ressources-humaines/la-place-du-benevole-dans-lassociation/)
        * [x]   [Recrutement du Comité local d’organisation](https://www.organiser-un.tfjm.org/ressources-de-formation/ressources-humaines/recrutement-du-comite-local-dorganisation/) {>>Vide<<}
    * [ ]   [Outils en ligne](https://www.organiser-un.tfjm.org/ressources-de-formation/outils-en-ligne/)
        * [x]   [Limesurvey (questionnaires)](https://www.organiser-un.tfjm.org/ressources-de-formation/outils-en-ligne/limesurvey-questionnaires/) {>>Vide<<}
        * [x]   [Own Cloud (drive)](https://www.organiser-un.tfjm.org/ressources-de-formation/outils-en-ligne/own-cloud-drive/) {>>Vide<<}
        * [x]   [Plateforme d’inscription](https://www.organiser-un.tfjm.org/ressources-de-formation/outils-en-ligne/plateforme-dinscription/) {>>Vide<<}
        * [x]   [Plateforme de tickets](https://www.organiser-un.tfjm.org/ressources-de-formation/outils-en-ligne/plateforme-de-tickets/) {>>Vide<<}
        * [x]   [Tirages au sort](https://www.organiser-un.tfjm.org/ressources-de-formation/outils-en-ligne/tirages-au-sort/) {>>Vide<<}
        * [x]   [www.tfjm.org](https://www.organiser-un.tfjm.org/ressources-de-formation/outils-en-ligne/www-tfjm-org/) {>>Vide<<}
    * [x]   [Communication](https://www.organiser-un.tfjm.org/ressources-de-formation/communication/) {>>Vide<<}
        * [x]   [Communication locale en amont](https://www.organiser-un.tfjm.org/ressources-de-formation/communication/communication-locale-en-amont/) {>>Vide<<}
        * [x]   [Communication locale pendant le tournoi](https://www.organiser-un.tfjm.org/ressources-de-formation/communication/communication-locale-pendant-le-tournoi/) {>>Vide<<}
    * [x]   [Partenariats](https://www.organiser-un.tfjm.org/ressources-de-formation/partenariats/) {>>Vide<<}
        * [x]   [Institution support](https://www.organiser-un.tfjm.org/ressources-de-formation/partenariats/institution-support/) {>>Vide<<}
        * [x]   [Recherche de financements (local)](https://www.organiser-un.tfjm.org/ressources-de-formation/partenariats/recherche-de-financements-local/)

De nombreuses pages sur le site original étaient vides, mais faisaient partie de l'idée originelle d'Antoine Martin.
Certaines n'ont pas de nécessité à exister, mais d'autres méritent l'existence. Si tu as envie d'en écrire une, n'hésite pas,
même si c'est une petite page.

